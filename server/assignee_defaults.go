package main

import (
	"net/http"

	"fmt"

	"golang.org/x/net/context"
	"google.golang.org/appengine"
	"google.golang.org/appengine/datastore"
	"google.golang.org/appengine/log"
	"google.golang.org/appengine/user"
)

func createDefaultAssigneeConfiguration(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	u := user.Current(c)
	if u == nil {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	if !u.Admin {
		w.WriteHeader(http.StatusForbidden)
		return
	}
	config, err := getAssigneeConfigurationFromDatastore(c)
	if err != nil {
		log.Errorf(c, "Error querying current configuration: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if len(config.Assignees) > 0 || len(config.Groups) > 0 {
		w.WriteHeader(http.StatusConflict)
		fmt.Fprintf(w, "Configuration already present")
		return
	}

	err = datastore.RunInTransaction(c, func(c context.Context) error {
		log.Debugf(c, "Creating configuration root")
		_, xerr := createConfigurationRoot(c)
		if xerr != nil {
			return xerr
		}
		log.Debugf(c, "Creating default groups")
		groups, xerr := insertDefaultGroups(c)
		if xerr != nil {
			return xerr
		}
		log.Debugf(c, "Creating default assignees")
		_, xerr = insertDefaultAssignees(c, groups)
		return xerr
	}, nil)
	if err != nil {
		log.Errorf(c, "Error populating database: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	fmt.Fprintf(w, "OK")
}

func insertDefaultGroups(c context.Context) ([]AssigneeGroup, error) {
	var groups = []AssigneeGroup{
		AssigneeGroup{Name: "First", SortOrder: 0},
		AssigneeGroup{Name: "Angular (Internal)", SortOrder: 1},
		AssigneeGroup{Name: "Angular (External)", SortOrder: 2},
		AssigneeGroup{Name: "JIRA (Internal)", SortOrder: 3},
		AssigneeGroup{Name: "JIRA (External)", SortOrder: 4},
		AssigneeGroup{Name: "Last", SortOrder: 5},
	}
	for i := range groups {
		log.Infof(c, "Inserting group: %v", groups[i])
		err := insertAssigneeGroup(c, &groups[i])
		if err != nil {
			log.Errorf(c, "Error inserting default group: %v", err)
			return nil, err
		}
	}
	return groups, nil
}

func insertDefaultAssignees(c context.Context, groups []AssigneeGroup) ([]Assignee, error) {
	var assignees = []Assignee{
		Assignee{Key: "jack.balbes", Hidden: false, Groups: []*datastore.Key{groups[1].ID}},
		Assignee{Key: "john.baur", Hidden: false, Groups: []*datastore.Key{groups[1].ID}},
		Assignee{Key: "paul.spears", Hidden: false, Groups: []*datastore.Key{groups[1].ID}},
		Assignee{Key: "kyle.cordes", Hidden: false, Groups: []*datastore.Key{groups[1].ID}},
		Assignee{Key: "maneesh.tewani", Hidden: false, Groups: []*datastore.Key{groups[1].ID}},
		Assignee{Key: "rachel.noccioli", Hidden: false, Groups: []*datastore.Key{groups[1].ID}},
		Assignee{Key: "david.moore", Hidden: false, Groups: []*datastore.Key{groups[2].ID}},
		Assignee{Key: "cory.rylan", Hidden: false, Groups: []*datastore.Key{groups[2].ID}},
		Assignee{Key: "bill.odom", Hidden: false, Groups: []*datastore.Key{groups[2].ID}},
		Assignee{Key: "joshuagodi", Hidden: false, Groups: []*datastore.Key{groups[2].ID}},
		Assignee{Key: "lynn.mcneil", Hidden: false, Groups: []*datastore.Key{groups[3].ID}},
		Assignee{Key: "michael.mcneil", Hidden: false, Groups: []*datastore.Key{groups[3].ID}},
		Assignee{Key: "robert.anthony", Hidden: false, Groups: []*datastore.Key{groups[4].ID}},
		Assignee{Key: "steve.terelmes", Hidden: false, Groups: []*datastore.Key{groups[4].ID}},
		Assignee{Key: "ally.nissen", Hidden: true, Groups: []*datastore.Key{}},
		Assignee{Key: "$$unassigned-angular", Name: "(Unassigned Angular)", Groups: []*datastore.Key{}},
		Assignee{Key: "$$unassigned-jira", Name: "(Unassigned JIRA)", Groups: []*datastore.Key{}},
	}
	for i := range assignees {
		err := insertAssignee(c, &assignees[i])
		if err != nil {
			log.Errorf(c, "Error inserting default assignee: %v", err)
			return nil, err
		}
	}
	return assignees, nil
}
