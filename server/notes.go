package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"time"

	"golang.org/x/net/context"
	"google.golang.org/appengine"
	"google.golang.org/appengine/datastore"
	"google.golang.org/appengine/log"
)

type WeekNotes struct {
	Week       string    `json:"week"`
	Instructor string    `json:"instructor"`
	Notes      string    `json:"notes"`
	EnteredBy  string    `json:"enteredBy"`
	AsOf       time.Time `json:"asOf"`
}

func saveNotes(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	u := auth(c, w, r)
	if u == nil {
		return
	}

	if !u.Rights.EnterNotes {
		w.WriteHeader(http.StatusForbidden)
		return
	}

	defer r.Body.Close()
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Errorf(c, "Error reading request: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var note WeekNotes
	err = json.Unmarshal(body, &note)
	if err != nil {
		log.Errorf(c, "Error unmarshaling request (%v): %v", body, err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	note.EnteredBy = u.User.Email
	note.AsOf = time.Now()

	_, err = datastore.Put(c, datastore.NewIncompleteKey(c, "WeekNote", nil), &note)
	if err != nil {
		log.Errorf(c, "Error saving note: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func getNotes(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	u := auth(c, w, r)
	if u == nil {
		return
	}

	if !u.Rights.SeeAllClasses && !u.Rights.SeeOwnClasses {
		http.Error(w, "You do not have the permission to see classes and notes", http.StatusUnauthorized)
		return
	}

	var notes []WeekNotes
	_, err := datastore.NewQuery("WeekNote").GetAll(c, &notes)
	if err != nil {
		log.Errorf(c, "Error getting notes from datastore: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	notes, err = authorizeNotes(notes, c, u)
	if err != nil {
		log.Errorf(c, "Error authorizing: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(notes)
}

func authorizeNotes(data []WeekNotes, c context.Context, u *UserWithRights) ([]WeekNotes, error) {
	if u.Rights.SeeAllClasses {
		log.Debugf(c, "Can see all notes, returning as is")
		return data, nil
	}
	authorized := []WeekNotes{}
	if u.Rights.SeeOwnClasses {
		for _, note := range data {
			log.Debugf(c, "Authorizing note, comparing %v to %v", note.Instructor, u.User.Email)
			if note.Instructor == u.User.Email {
				authorized = append(authorized, note)
			}
		}
	}

	return authorized, nil
}
