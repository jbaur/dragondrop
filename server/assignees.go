package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"golang.org/x/net/context"
	"google.golang.org/appengine"
	"google.golang.org/appengine/datastore"
	"google.golang.org/appengine/log"
)

type Assignee struct {
	ID     *datastore.Key   `json:"id" datastore:"-"`
	Key    string           `json:"key"`
	Name   string           `json:"name"`
	Email  string           `json:"email"`
	Groups []*datastore.Key `json:"groups"`
	Hidden bool             `json:"hidden"`
}

type AssigneeGroup struct {
	ID        *datastore.Key `json:"id" datastore:"-"`
	Name      string         `json:"name"`
	SortOrder int            `json:"sortOrder"`
}

type AssigneesAndGroups struct {
	Assignees []Assignee      `json:"assignees"`
	Groups    []AssigneeGroup `json:"groups"`
}

type InputAssignee struct {
	ID         *datastore.Key   `json:"id" datastore:"-"`
	Key        string           `json:"key"`
	Name       string           `json:"name"`
	Email      string           `json:"email"`
	Groups     []*datastore.Key `json:"groups"`
	Hidden     bool             `json:"hidden"`
	TempGroups []string         `json:"tempGroups"`
}

type InputAssigneeGroup struct {
	ID        *datastore.Key `json:"id" datastore:"-"`
	Name      string         `json:"name"`
	SortOrder int            `json:"sortOrder"`
	TempID    string         `json:"tempId"`
}

type InputAssigneesAndGroups struct {
	Assignees []InputAssignee      `json:"assignees"`
	Groups    []InputAssigneeGroup `json:"groups"`
}

// AssigneeConfiguration for now has no attributes. It's used as the root entity for Assignee and AssigneeGroup in order to provide transactional consistency
type AssigneeConfiguration struct {
}

func assigneeConfigurationKey(c context.Context) *datastore.Key {
	return datastore.NewKey(c, "AssigneeConfiguration", "AssigneeConfiguration", 0, nil)
}

func createConfigurationRoot(c context.Context) (*datastore.Key, error) {
	return datastore.Put(c, assigneeConfigurationKey(c), &AssigneeConfiguration{})
}

func createConfigurationRootIfNeeded(c context.Context) (*datastore.Key, error) {
	existing := AssigneeConfiguration{}
	key := assigneeConfigurationKey(c)
	err := datastore.Get(c, key, &existing)
	if err == datastore.ErrNoSuchEntity {
		return createConfigurationRoot(c)
	}
	if err != nil {
		return nil, err
	}
	return key, nil
}

func getAssigneeConfiguration(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	u := auth(c, w, r)
	if u == nil {
		return
	}

	config, err := getAssigneeConfigurationFromDatastore(c)
	if err != nil {
		log.Errorf(c, "Error getting assignee configuration: %v", err)
		config = &AssigneesAndGroups{Assignees: []Assignee{}, Groups: []AssigneeGroup{}}
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(*config)
}

func getAssigneeConfigurationFromDatastore(c context.Context) (*AssigneesAndGroups, error) {
	root := assigneeConfigurationKey(c)
	var assignees []Assignee
	keys, err := datastore.NewQuery("Assignee").Ancestor(root).Order("Key").GetAll(c, &assignees)
	if err != nil {
		return nil, err
	}
	for i := range assignees {
		assignees[i].ID = keys[i]
	}
	var groups []AssigneeGroup
	keys, err = datastore.NewQuery("AssigneeGroup").Ancestor(root).Order("SortOrder").GetAll(c, &groups)
	if err != nil {
		return nil, err
	}
	for i := range groups {
		groups[i].ID = keys[i]
	}
	if assignees == nil {
		assignees = []Assignee{}
	}
	if groups == nil {
		groups = []AssigneeGroup{}
	}
	return &AssigneesAndGroups{Assignees: assignees, Groups: groups}, nil
}

func updateAssigneeConfiguration(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	u := auth(c, w, r)
	if u == nil {
		return
	}

	if !u.Rights.Admin {
		w.WriteHeader(http.StatusForbidden)
		return
	}

	defer r.Body.Close()
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Errorf(c, "Error reading request: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	var newState InputAssigneesAndGroups
	err = json.Unmarshal(body, &newState)
	if err != nil {
		log.Errorf(c, "Error unmarshaling request (%v): %v", body, err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	currentState, err := getAssigneeConfigurationFromDatastore(c)
	if err != nil {
		log.Errorf(c, "Error loading from DB: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = datastore.RunInTransaction(c, func(c context.Context) error {
		var xerr error
		currentState, xerr = updateAssigneeConfigurationInTransaction(c, currentState, &newState)
		return xerr
	}, nil)
	if err != nil {
		log.Errorf(c, "Error saving: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(currentState)
}

func updateAssigneeConfigurationInTransaction(c context.Context, currentState *AssigneesAndGroups, newState *InputAssigneesAndGroups) (*AssigneesAndGroups, error) {
	_, err := createConfigurationRootIfNeeded(c)
	if err != nil {
		return nil, err
	}

	resultGroups, groupIDMapping, err := updateGroups(c, currentState, newState)
	if err != nil {
		return nil, err
	}

	resultAssignees, err := updateAssignees(c, currentState, newState, groupIDMapping)

	return &AssigneesAndGroups{Groups: resultGroups, Assignees: resultAssignees}, nil
}

func updateGroups(c context.Context, currentState *AssigneesAndGroups, newState *InputAssigneesAndGroups) ([]AssigneeGroup, map[string]*datastore.Key, error) {
	existingGroups := currentState.Groups
	newGroups := newState.Groups

	resultGroups := []AssigneeGroup{}
	groupIDMapping := make(map[string]*datastore.Key)

	// Create or update existing groups
	for i, ng := range newGroups {
		if ng.TempID != "" {
			newGroup := AssigneeGroup{Name: ng.Name, SortOrder: i}
			log.Infof(c, "Inserting group: %v", newGroup)
			err := insertAssigneeGroup(c, &newGroup)
			if err != nil {
				log.Errorf(c, "Error inserting group: %v", err)
				return nil, nil, err
			}
			groupIDMapping[ng.TempID] = newGroup.ID
			resultGroups = append(resultGroups, newGroup)
		} else {
			for j := range existingGroups {
				if existingGroups[j].ID.Equal(ng.ID) {
					if existingGroups[j].Name != ng.Name || existingGroups[j].SortOrder != i {
						newGroup := AssigneeGroup{Name: ng.Name, SortOrder: i, ID: ng.ID}
						err := updateAssigneeGroup(c, &newGroup)
						if err != nil {
							return nil, nil, err
						}
						resultGroups = append(resultGroups, newGroup)
					} else {
						resultGroups = append(resultGroups, existingGroups[j])
					}
					break
				}
			}
		}
	}

	// Remove groups that no longer exist
	for _, eg := range existingGroups {
		found := false
		for _, ng := range resultGroups {
			if eg.ID.Equal(ng.ID) {
				found = true
				break
			}
		}
		if !found {
			log.Infof(c, "Deleting group: %v", eg)
			err := datastore.Delete(c, eg.ID)
			if err != nil {
				log.Errorf(c, "Error deleting group: %v", err)
				return nil, nil, err
			}
		}
	}
	return resultGroups, groupIDMapping, nil
}

func updateAssignees(c context.Context, currentState *AssigneesAndGroups, newState *InputAssigneesAndGroups, groupIDMapping map[string]*datastore.Key) ([]Assignee, error) {
	existingAssignees := make([]Assignee, len(currentState.Assignees))
	copy(existingAssignees, currentState.Assignees)
	newAssignees := newState.Assignees

outer:
	for _, na := range newAssignees {
		newGroups := na.Groups
		for _, tempID := range na.TempGroups {
			newGroups = append(newGroups, groupIDMapping[tempID])
		}

		if na.ID == nil {
			assignee := Assignee{
				Key:    na.Key,
				Name:   na.Name,
				Email:  na.Email,
				Hidden: na.Hidden,
				Groups: newGroups,
			}
			err := insertAssignee(c, &assignee)
			if err != nil {
				return nil, err
			}
			existingAssignees = append(existingAssignees, assignee)
			continue
		}
		for j, ea := range existingAssignees {
			if na.ID.Equal(ea.ID) {

				hasChanges := false

				if na.Name != ea.Name {
					log.Debugf(c, "Name changed: %s -> %s", ea.Name, na.Name)
					existingAssignees[j].Name = na.Name
					hasChanges = true
				}
				if na.Email != ea.Email {
					log.Debugf(c, "Email changed: %s -> %s", ea.Email, na.Email)
					existingAssignees[j].Email = na.Email
					hasChanges = true
				}
				if na.Hidden != ea.Hidden {
					log.Debugf(c, "Hidden changed: %s -> %s", ea.Hidden, na.Hidden)
					existingAssignees[j].Hidden = na.Hidden
					hasChanges = true
				}
				groupChanged := len(newGroups) != len(ea.Groups)
				if groupChanged {
					log.Debugf(c, "Group changed (length): %s (%d) -> %s (%d)", ea.Groups, len(ea.Groups), newGroups, len(newGroups))
					existingAssignees[j].Groups = newGroups
				} else {
				groupLoop:
					for _, a := range newGroups {
						for _, b := range ea.Groups {
							if *a == *b {
								continue groupLoop
							}
						}
						existingAssignees[j].Groups = newGroups
						log.Debugf(c, "Group changed (contents): %s -> %s", ea.Groups, newGroups)
						groupChanged = true
					}
				}
				hasChanges = hasChanges || groupChanged

				if hasChanges {
					err := updateAssignee(c, &existingAssignees[j])
					if err != nil {
						return nil, err
					}
				}

				continue outer
			}
		}
	}

	return existingAssignees, nil
}

// WARNING: This assumes the root entity has already been created with createConfigurationRoot(c)
func insertAssignee(c context.Context, assignee *Assignee) error {
	log.Infof(c, "Inserting assignee: %v", assignee)
	key, err := datastore.Put(c, datastore.NewIncompleteKey(c, "Assignee", assigneeConfigurationKey(c)), assignee)
	if err != nil {
		log.Errorf(c, "Error inserting assignee: %v", err)
		return err
	}
	assignee.ID = key
	return nil
}

func updateAssignee(c context.Context, assignee *Assignee) error {
	log.Infof(c, "Updating assignee: %v", assignee)
	_, err := datastore.Put(c, assignee.ID, assignee)
	if err != nil {
		log.Errorf(c, "Error updating assignee: %v", err)
	}
	return err
}

// WARNING: This assumes the root entity has already been created with createConfigurationRoot(c)
func insertAssigneeGroup(c context.Context, group *AssigneeGroup) error {
	log.Infof(c, "Inserting assignee group: %v", group)
	key, err := datastore.Put(c, datastore.NewIncompleteKey(c, "AssigneeGroup", assigneeConfigurationKey(c)), group)
	if err != nil {
		log.Errorf(c, "Error inserting assignee group: %v", err)
		return err
	}
	group.ID = key
	return nil
}

func updateAssigneeGroup(c context.Context, group *AssigneeGroup) error {
	log.Infof(c, "Updating assignee: %v", group)
	_, err := datastore.Put(c, group.ID, group)
	if err != nil {
		log.Errorf(c, "Error updating assignee group: %v", err)
	}
	return err
}
