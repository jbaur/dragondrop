package main

import (
	"fmt"
	"net/http"
	"regexp"

	"golang.org/x/net/context"
	"google.golang.org/appengine/datastore"
	"google.golang.org/appengine/log"
	"google.golang.org/appengine/user"
)

type PrincipalRights struct {
	EmailRegexp    string `json:",omitempty"`
	SeeOwnClasses  bool
	SeeAllClasses  bool
	EnterNotes     bool
	SeeFullHistory bool
	Admin          bool
}

type UserWithRights struct {
	User   *user.User
	Rights *PrincipalRights
}

func (pr PrincipalRights) String() string {
	return fmt.Sprintf("PrincipalRights{EmailRegexp=%v, SeeOwnClasses=%v, SeeAllClasses=%v, EnterNotes=%v, SeeFullHistory=%v, Admin=%v}", pr.EmailRegexp, pr.SeeOwnClasses, pr.SeeAllClasses, pr.EnterNotes, pr.SeeFullHistory, pr.Admin)
}

// Returns user (if present and authorized), or dummy user with Email=ANONYMOUS.
// Does not return any response to client.
func getUserWithRights(c context.Context, w http.ResponseWriter, r *http.Request) (*UserWithRights, error) {
	u := user.Current(c)
	if u == nil {
		log.Infof(c, "Trying anonymous authentication")
		u = &user.User{Email: ANONYMOUS}
	}
	rights, err := getRights(c, u)
	if err != nil {
		return nil, err
	}

	return &UserWithRights{User: u, Rights: rights}, nil
}

// Returns user (if present and authorized), or dummy user with Email=ANONYMOUS.
// Responds with 401 when access denied.
func auth(c context.Context, w http.ResponseWriter, r *http.Request) *UserWithRights {
	uwr, err := getUserWithRights(c, w, r)
	if err != nil {
		log.Errorf(c, "Error getting user rights: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return nil
	}
	if uwr.Rights == nil {
		log.Infof(c, "Access denied for %s", uwr.User.Email)
		http.Error(w, "Access denied", http.StatusUnauthorized)
		return nil
	}

	log.Infof(c, "Authenticated as %s", uwr.User.Email)
	return uwr
}

// Returns rights applicable to this user, or nil if there are none. In other words,
// nil indicates an account that does not even exist in our system (and should be
// denied entry).
func getRights(c context.Context, u *user.User) (*PrincipalRights, error) {
	rights, err := queryRights(c)
	if err != nil {
		return nil, err
	}
	var result *PrincipalRights
	for _, pr := range rights {
		match, _ := regexp.MatchString(pr.EmailRegexp, u.Email)
		log.Debugf(c, "Checking principal rights %v, matching: %v", pr, match)
		if match {
			if result == nil {
				result = &PrincipalRights{}
			}
			result.EnterNotes = result.EnterNotes || pr.EnterNotes
			result.SeeAllClasses = result.SeeAllClasses || pr.SeeAllClasses
			result.SeeOwnClasses = result.SeeOwnClasses || pr.SeeOwnClasses
			result.SeeFullHistory = result.SeeFullHistory || pr.SeeFullHistory
			result.Admin = result.Admin || pr.Admin
		}
	}
	log.Infof(c, "Effective rights: %v", result)
	return result, nil
}

// Returns all rights configured in the system. If none exist, it will create a new
// permission for everyone in @oasisdigital.com to see their own classes. This also
// acts as a good demonstration/starting point.
func queryRights(c context.Context) ([]PrincipalRights, error) {
	var rights []PrincipalRights
	_, err := datastore.NewQuery("PrincipalRights").GetAll(c, &rights)
	if err != nil {
		return nil, err
	}
	if len(rights) == 0 {
		// Insert placeholder if there are no such entities
		right := PrincipalRights{
			EmailRegexp:    ".*@oasisdigital.com",
			Admin:          false,
			EnterNotes:     false,
			SeeAllClasses:  false,
			SeeFullHistory: false,
			SeeOwnClasses:  true,
		}
		_, err = datastore.Put(c, datastore.NewIncompleteKey(c, "PrincipalRights", nil), &right)
		if err != nil {
			return nil, err
		}
		rights = []PrincipalRights{right}
	}

	return rights, nil
}
