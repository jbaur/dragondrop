import { createStore, combineReducers, ReducersMapObject, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootSaga from './sagas/index';
import { forIn } from 'lodash';

import Data from './data-state/store';
import Filter from './filter-state/store';
import Notes from './notes-state/store';
import Assignees from './assignees-state/store';
import { Assignee } from './../app/data-types';

const reducers: ReducersMapObject = {
  data: Data,
  filter: Filter,
  notesStorage: Notes,
  assignees: Assignees
};

const ADD_NEW_GROUP = 'ADD_NEW_GROUP';
export const addNewGroup = (name: string) => {
  return {
    type: ADD_NEW_GROUP,
    payload: name
  };
};

const rootReducer = combineReducers(reducers);

const appReducer = (prevState: any, action: any) => {
  const state = <any>rootReducer(prevState, action);
  switch (action.type) {
    case ADD_NEW_GROUP:
      const stateClone = { ...state };
      const group = action.payload;
      if (!stateClone.filter.groups[group]) {
        forIn(stateClone.filter.groups, (g: any) => g.enabled = false);
        stateClone.filter.groups[group] = {
          name: group,
          enabled: true,
          user: true
        };
        stateClone.assignees.groups.push({
          id: group,
          name: group
        });
        stateClone.assignees.assignees.forEach((ga: Assignee, gidx: number) => {
          if (ga.groups) {
            ga.groups.push(group);
          } else {
            ga.groups = [group];
          }
        });
      }
      return stateClone;
    default:
      return state;
  }
}

export default function configureStore(initialState: {}) {
  const sagaMiddleware = createSagaMiddleware();
  return {
    ...createStore(appReducer, initialState, applyMiddleware(sagaMiddleware)),
    runSaga: sagaMiddleware.run(rootSaga)
  };
}
