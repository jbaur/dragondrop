import * as types from '../action-types';
import { Assignee } from '../../app/data-types';

interface ActionInterface {
  type: string;
  payload: any;
}

interface StateInterface {
  defaultAssignees: Assignee[];
  assignees: Assignee[];
  groups: any[];
  myself: boolean;
  identity: any;
}

const defaultState: StateInterface = {
  defaultAssignees: [],
  assignees: [],
  groups: [],
  myself: false,
  identity: { User: {}, Rights: {} }
};

const MOVE_INSTRUCTOR = 'MOVE_INSTRUCTOR';
export const moveInstructor = (data: any) => {
  return {
    type: MOVE_INSTRUCTOR,
    payload: data
  };
};

const REMOVE_INSTRUCTOR = 'REMOVE_INSTRUCTOR';
export const removeInstructor = (data: any) => {
  return {
    type: REMOVE_INSTRUCTOR,
    payload: data
  };
};

export default (state: StateInterface = defaultState, action: ActionInterface) => {
  switch (action.type) {
    case types.RETRIEVE_ASSIGNEES_SUCCESS:
      return { ...action.payload, defaultAssignees: [ ...action.payload.assignees ] };
    case MOVE_INSTRUCTOR:
      return reorderInstructors(action.payload, state);
    case REMOVE_INSTRUCTOR:
      let assignees = [...state.assignees];
      assignees.splice(action.payload,1);
      return {...state, assignees};
    case 'TOGGLE_GROUP_ITEM':
      return { ...state, assignees: [ ...state.defaultAssignees ] };
    default:
      return state;
  }
};

function reorderInstructors (data: {old: number, new: number}, state: any) {
  let list = [...state.assignees];
  if (data.new >= list.length) {
    var k = data.new - list.length;
    while ((k--) + 1) {
        list.push(undefined);
    }
  }
  if (data.new > data.old) {
    list.splice(data.new-1, 0, list.splice(data.old, 1)[0]);
  } else {
    list.splice(data.new, 0, list.splice(data.old, 1)[0]);
  }
  return {...state, assignees: list};
};
