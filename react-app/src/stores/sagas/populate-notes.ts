import { takeLatest, put, call } from 'redux-saga/effects';
import axios from 'axios';
import * as types from '../action-types';

const getData = () => {
  return (
    axios.get('/api/notes').then((resp: any) => {
      return resp.data;
    })
  );
};

export default function* setData() {
  yield takeLatest(types.RETRIEVE_NOTES_REQUEST, dataSaga);
}

function* dataSaga(action: any) {
  try {
    const data = yield call(getData);
    yield put({
      type: types.RETRIEVE_NOTES_SUCCESS,
      payload: data
    });
  } catch (err) {
    yield put({
      type: types.RETRIEVE_NOTES_ERROR,
      payload: err
    });
  }
}
