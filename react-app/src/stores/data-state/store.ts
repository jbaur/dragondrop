import * as types from '../action-types';

import { Data, Class, Assignee } from './../../app/data-types';

interface StateInterface {
  data: Data;
}

interface ActionInterface {
  type: string;
  payload: any;
}

export const setData = (data: any) => {
  return {
    type: 'SET_DATA',
    payload: data
  };
};

export interface classReassignment {
  classInfo: Class;
  oldAssignee: Assignee
  newAssignee: Assignee;
}

const REASSIGN_CLASS = 'REASSIGN_CLASS';
export const reassignClass = (payload: classReassignment) => {
  return {
    type: REASSIGN_CLASS,
    payload
  };
}

const defaultState: StateInterface = {
  data: <Data> {}
};

export default (state: any = defaultState, action: ActionInterface) => {
  switch (action.type) {
    case types.RETRIEVE_DATA_SUCCESS:
      return action.payload;
    case types.RETRIEVE_DATA_ERROR:
      return action.payload;
    case REASSIGN_CLASS:
      console.log('reassign', state, action.payload);
      const { classInfo, oldAssignee, newAssignee } = action.payload;
      const newState = { ...state };
      const classIdx = newState.classes.findIndex((c: Class) => c.key === classInfo.key);
      const assigneeIdx = newState.classes[classIdx].instructors.findIndex((a: Assignee) => a.key === oldAssignee.key);
      newState.classes[classIdx].instructors[assigneeIdx] = { ...newAssignee };
      return newState;
    default:
      return state;
  }
};
