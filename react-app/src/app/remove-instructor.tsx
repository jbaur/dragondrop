import * as React from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { DropTarget } from 'react-dnd';
import { Glyphicon } from 'react-bootstrap';

import {  dragTypes } from './data-types';
import { removeInstructor } from '../stores/assignees-state/store';

class RemoveInstructor extends React.Component<any, any> {
    render() {
        const { canDrop, connectDropTarget } = this.props;

        return connectDropTarget (
            <div>{canDrop && <Glyphicon glyph={'trash'} style={{fontSize: '24px', color: 'dark-grey', marginRight: '50px', marginTop: '5px'}} />}</div>
        )
    }
}

const classTarget = {
  canDrop(props: any, monitor: any) {
    // const item = monitor.getItem();
    return true;
  },

  drop(props: any, monitor: any, component: any) {
    props.removeInstructor(monitor.getItem().passed.index);
  }
};

/**
 * Specifies which props to inject into your component.
 */
function collect(connect: any, monitor: any) {
  return {
    // Call this function inside render()
    // to let React DnD handle the drag events:
    connectDropTarget: connect.dropTarget(),
    // You can ask the monitor about the current drag state:
    isOver: monitor.isOver(),
    isOverCurrent: monitor.isOver({ shallow: true }),
    canDrop: monitor.canDrop(),
    itemType: monitor.getItemType()
  };
}

// Maps the states to props on NavMenu
const mapStateToProps = (state: any, ownProps: any) => {
  return {
    ...ownProps
  };
};

// Maps dispatch functions to NavMenu
const mapDispatchToProps = (dispatch: Function) => {
  return {
      removeInstructor: (payload: any) => dispatch(removeInstructor(payload))
  };
};

// Provides wrapper for NavMenu, and maps the given props from mapStateToProps and mapDispatchToProps

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    DropTarget(dragTypes.INSTRUCTOR, classTarget, collect)
)(RemoveInstructor)
