import * as React from 'react';
import { Glyphicon } from 'react-bootstrap';
import * as _ from 'lodash';
import { DragSource } from 'react-dnd';

import ClassLocation from './class-location';
import { Class, Assignee, dragTypes } from '../data-types';

interface EGCProps {
  [key: string]: any;
  classInfo: Class;
  instructor: Assignee | any;
  detailedView: any;
  onClick: Function;
}

class EngagementGridClass extends React.Component<EGCProps, any> {
  constructor(props: EGCProps) {
    super(props);
    this.state = this.props.classInfo;
  }

  // Used to setstate after component mount to prevent errors
  componentDidMount() {
    let sc = this.state.statusClass || '';
    switch (this.state.status) {
      case 'Decide Date':
      case 'Discussing Dates':
        sc = 'discussing-dates';
        break;
      case 'Downpayment':
      case 'Invoiced (Billed)':
        sc = 'payment-pending';
        break;
      case 'Reserve Facility':
        sc = 'need-to-reserve';
        break;
      case 'Booked Class (Deposit)':
      case 'Booked Class (PO Only)':
      case 'Class Scheduled':
        sc = 'booked';
        break;
      case 'Class Completed':
      case 'Closed':
        sc = 'completed';
        break;
      default:
        break;
    }
    this.setState({
      statusClass: sc,
      instructorPosition: this.props.instructor.key !== '(Unassigned)' &&
      _.findIndex(this.props.classInfo.instructors, { key: this.props.instructor.key })
    });
  }

  // Returnes proper glyphicon given the travel variable
  getTravelGlyph = () => {
    switch (this.props.instructor.travel) {
      case 'online':
        return 'flash';
      case 'booking-needed':
        return 'send';
      case 'booked':
        return 'plane';
      default:
        return '';
    }
  }

  // Returnes proper view given the state of detailedView
  getTicketSeat = () => {
    return (!this.props.detailedView) ? (
      <span className={'tickets-seats instructor' + this.state.instructorPosition} title={'Seats: ' + this.state.ticketsSeats || '?' + '&#10;Instructor ' + this.state.instructorPosition + 1 + ' of ' + this.state.instructors.length}>
        {(this.state.ticketsSeats)}
      </span>
    ) : (
        <span className={'tickets-seats instructor' + this.state.instructorPosition} title={'Seats: ' + this.state.ticketsSeats || '?' + '&#10;Instructor ' + this.state.instructorPosition + 1 + ' of ' + this.state.instructors.length}>
          {this.state.ticketsSeat + this.state.unassigned || '/' + this.state.instructors.length}
        </span>
      );
  }

  // Returnes proper view given the state of detailedView
  getInfo = () => {
    if (!this.props.detailedView) {
      return (
        <div>
          {
            (this.state.customerName) ?
              <div>
                <div className="half half-left">
                  <ClassLocation classInfo={this.props.classInfo} />
                </div>
                <div className="half half-right">{this.state.customerName}</div>
              </div>
              :
              <ClassLocation classInfo={this.props.classInfo} />
          }
        </div>
      );
    } else {
      return (
        <div className="detailed-view">
          {!(this.state.datesOffered && this.state.statusClass === 'discussing-dates') || <div style={{ whiteSpace: 'pre-line' }}>{this.state.datesOffered}</div>}
          {!(this.state.online || this.state.classLocationRegion || this.state.timeZone) || (
            <div>
              <ClassLocation detailedView={true} classInfo={this.props.classInfo} />{!this.state.timeZone || <span> ({this.state.timeZone}) </span>}
            </div>
          )}
          {!this.state.customerName || <div>{this.state.customerName}</div>}
          <div>{this.state.statusAbbr}</div>
          <div><a href={this.state.url} target="_blank"><Glyphicon glyph="share-alt" />{this.state.key}</a></div>
        </div>
      );
    }
  }

  // Creates the logo to be displayed in upper left corner of grid-class
  getClassType = () => {
    return [
      !this.state.angular || <span key="angular" className="logo a-logo" />,
      !this.state.angular2 || <span key="angular2" className="logo a2-logo" />,
      !this.state.jira || <span key="jira" className="logo jira-logo" />,
      !this.state.atlassianAcademy || <span key="atlassianAcademy" className="logo atlassian-academy-logo" />
    ];
  }

  render() {
    const { connectDragSource } = this.props;

    return connectDragSource(
      <div onClick={(event: any) => this.props.onClick(event)} className={'text-center training-class ' + this.state.statusClass + !this.state.public || ' public'} title={this.state.tooltip}>
        <div>
          {this.getClassType()}
          {this.getTicketSeat()}
          <div className="travel-icon">
            <Glyphicon glyph={this.getTravelGlyph()} />
          </div>
          <span className="class-date"><strong>{this.state.classDates}</strong></span>
          {this.getInfo()}
        </div>
      </div>
    );
  }
}

const classSource = {
  beginDrag(props: any) {
    return props;
  }
}

function collect(connect: any, monitor: any) {
  return {
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
  }
}

export default DragSource(dragTypes.CLASS, classSource, collect)(EngagementGridClass);
