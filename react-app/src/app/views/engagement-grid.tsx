import * as React from 'react';
import * as moment from 'moment';
import * as _ from 'lodash';
import axios from 'axios';
import * as Radium from 'radium';
import { connect } from 'react-redux';
import { Week, Assignee, Class } from '../data-types';

import { populateNotes } from '../../stores/notes-state/store';
import Error from './grid-error';
import MapInstructors from './grid-map-instructors';
import WeekMap from './grid-week-map';

export const dateUtil = {
  mDateOnly: function mDateOnly(date: any) {
    return moment(moment(date).format('YYYY-MM-DD'));
  },

  mWeekStart: function mWeekStart(date: any) {
    return dateUtil.mDateOnly(date).day(1);
  }
};

class EngagementGrid extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      scrollLeft: 0,
      status: '',
      error: {},
      classes: {},
      weeks: [],
      instructors: [],
      noteIndex: [],
      viewSettings: {
        detailedView: false,
        rowExpansion: [],
        isExpanded: (row: number) => {
          let vs = this.state.viewSettings;
          return typeof vs.rowExpansion[row] === 'boolean' ?
            vs.rowExpansion[row] : vs.detailedView;
        },
        toggleRowExpansion: (row: number, event: any) => {
          if (event.target.tagName !== 'A') {
            let clone = _.clone(this.state.viewSettings);
            clone.rowExpansion[row] = !this.state.viewSettings.isExpanded(row);
            this.setState({ viewSettings: clone });
          }
        }
      }
    };
  }

  // newProps needed. This function is called before this.props is updated
  componentWillReceiveProps(newProps: any) {
    this.loadNotes(newProps);
  }

  // Needed for when the route switches back to /grid. Doesn't load automatically otherwise
  // Only runs when component mounts
  componentDidMount() {
    this.loadNotes();
  }

  // Used to handle possible errors cause by null properties
  loadNotes = (newProps?: any) => {
    newProps = newProps || this.props;
    if (newProps.data.status === 'ERROR') {
      this.setState({status: 'ERROR', error: newProps.data.error});
    }
    if (newProps.data.classes && newProps.assignees.assignees) {
      this.reloadView(newProps.assignees.assignees, newProps.data.classes, newProps.notesStorage.notes, newProps);
    }
  }

  classInstructor = (classInfo: Class, instructor: Assignee) => {
    return _.find(classInfo.instructors, { key: instructor.key });
  }

  getClasses = (week: Week, instructor: Assignee) => {
    const classes = this.state.classes[week.weekStart || 'nodate'] || [];
    return classes[instructor.key] || [];
  }

  noteText = (w: Week, i: Assignee) => {
    return this.state.noteIndex[w.key + '-' + i.email] || '';
  }

  isEditable = (week: Week) => {
    return this.props.assignees.identity.Rights.EnterNotes && moment(week.weekStart).isAfter(dateUtil.mWeekStart(moment().subtract(5, 'weeks')));
  }

  needsThickBorder = (index: number, weeks: any) => {
    return index > 0 &&
      (
        weeks[index - 1].past && !weeks[index].past ||
        weeks[index].year ||
        weeks[index - 1].year
      );
  }

  needsHighlightYellow = (index: number, weeks: any) => {
    return dateUtil.mWeekStart(moment().add(4, 'weeks'))
      .format() === weeks[index].weekStart;
  }

  // Sets up the data in a usable format.
  // Is called after component mounts or props are updated
  reloadView = (_assignees: Assignee[], _engagements: Class[], notes: any, props: any) => {
    const groupByInstructor = (weekClasses: Class[]) => {
      let byInstructor = {};
      weekClasses.forEach((c: Class) => {
        if (props.filter.view === 3 || (c.public && props.filter.view === 1) || (!c.public && props.filter.view === 2)) {
          c.instructors.forEach((i: Assignee) => {
            if (!byInstructor[i.key]) {
              byInstructor[i.key] = [];
            }
            byInstructor[i.key].push(c);
          });
        }
      });
      return byInstructor;
    };
    if (_engagements.length === 0 && notes.length === 0) {
      if (props.data.classes.length > 0) {
        this.setState({ status: 'DATA_FILTERED' });
      } else {
        this.setState({ status: 'NO_DATA' });
      }
      return;
    } else {
      this.setState({ status: 'DATA_PRESENT' });
    }

    let weekStart;
    let lastWeek;
    let weekDates;
    const byWeek = _(_engagements)
      .filter((c: Class) => c.hasDate)
      .sortBy((c: Class) => c.startDateString + '-' + c.endDateString)
      .groupBy((c: Class) => dateUtil.mWeekStart(c.startDate).format())
      .map((weekClasses: Class[], week: Week) => [week, groupByInstructor(weekClasses)])
      .fromPairs()
      .value();
    const noDates = _(_engagements)
      .filter((c: Class) => !c.hasDate)
      .sortBy(['type', 'customerName'])
      .value();
    this.setState({ classes: Object.assign({ 'nodate': groupByInstructor(noDates) }, byWeek) });

    this.setState({ weeks: [] });

    weekDates = _.concat(Object.keys(byWeek), _.map(notes, 'week'));

    let newWeeks = [];

    if (weekDates.length > 0) {
      lastWeek = moment(_.max(weekDates)).add(4, 'weeks');
      weekStart = moment(_.min(weekDates));
      while (!weekStart.isAfter(lastWeek)) {
        if (newWeeks.length === 0 ||
          weekStart.format('YYYY') !== moment(weekStart).subtract(7, 'days').format('YYYY')) {
          newWeeks.push({ year: weekStart.format('YYYY') });
        }
        newWeeks.push({
          weekStart: weekStart.format(),
          weekEnd: moment(weekStart).add(6, 'days').format(),
          key: moment(weekStart).format('YYYY-MM-DD'),
          past: moment(weekStart).add(5, 'days').isBefore(new Date())
        });
        weekStart = moment(weekStart).add(7, 'days');
      }
    }

    newWeeks.push({});

    let viewSettingsClone = _.clone(this.state.viewSettings);
    viewSettingsClone.rowExpansion = [];
    this.setState({
      weeks: newWeeks,
      instructors: (props.filter.groups['everyone'].enabled) ? _assignees : _assignees.filter((assignee: any) => {
        let ret = false;
        if (props.filter.groups['just-me'].enabled && assignee.myself) {
          ret = true;
        }
        if (assignee.groups) {
          assignee.groups.forEach((id: string) => {
            ret = props.filter.groups[id].enabled || ret;
          });
        }
        return ret;
      }),
      viewSettings: viewSettingsClone,
      noteIndex: _(notes)
        .map(function (n: any) {
          return [n.week + (n.instructor ? '-' + n.instructor : ''), n.notes];
        })
        .fromPairs()
        .value()
    });

  }

  statusSwitch = () => {
    switch (this.state.status) {
      case 'LOADING':
        return (
          <div>
            <h4>Loading...</h4>
          </div>
        );
      case 'ERROR':
        return (
          <Error error={this.state.error} />
        );
      case 'NO_DATA':
        return (
          <div className="alert" style={{ margin: '15px' }}>
            You do not have any classes or notes.
          </div>
        );
      case 'DATA_FILTERED':
        return (
          <div className="alert" style={{ margin: '15px;' }}>
            You do not have any classes or notes matching the filter criteria.
          </div>
        );
      case 'DATA_PRESENT':
        return (
          <div className="panel panel-default">
            <table className="table table-condensed" id="grid">
              <thead>
                <MapInstructors instructors={this.state.instructors} />
              </thead>
              {/* Squish all passed properties into one property */}
              <WeekMap
                passed={{
                  weeks: this.state.weeks,
                  instructors: this.state.instructors,
                  noteIndex: this.state.noteIndex,
                  noteText: this.noteText,
                  viewSettings: this.state.viewSettings,
                  saveNotes: this.props.saveNotes,
                  toggleRowExpansion: this.state.viewSettings.toggleRowExpansion,
                  getClasses: this.getClasses,
                  isEditable: this.isEditable,
                  classInstructor: this.classInstructor,
                  needsHighlightYellow: this.needsHighlightYellow,
                  needsThickBorder: this.needsThickBorder,
                }}
              />
            </table>
          </div>
        );
      default:
        return (<h3>Loading Data...</h3>);
    }
  }

  render() {
    return this.statusSwitch();
  }
}

const postNotes = (week: any, instructor: any, notes: any) => {
  axios.post('/api/notes', {
    week: week,
    instructor: instructor,
    notes: notes
  });
};

const mapStateToProps = (state: any, ownProps: {}) => {
  return {
    ...ownProps,
    filter: state.filter,
    data: state.data,
    assignees: state.assignees,
    notesStorage: (() => { return state.notesStorage; })()
  };
};

// Saving notes. Currently unused due to lack of
// data in /api/notes
const mapDispatchToProps = (dispatch: Function) => {
  return {
    saveNotes: (week: any, instructor: any, notes: any) => {
      postNotes(week, instructor, notes);
      axios.get('/api/notes').then((response: any) => {
        setTimeout(() => { dispatch(populateNotes(response.data)); }, 5000);
      });
    }
  };
};

// Radium is used to enhance React inline-styling. Responsive x-browser css compatability
export default Radium(connect(mapStateToProps, mapDispatchToProps)(EngagementGrid));
