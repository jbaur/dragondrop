import * as React from 'react';

import { Assignee } from '../data-types';
import EngagementSquare from './engagement-square';
import Notes from './notes';

interface TableInstructorProps {
  passed: {
    instructors: Array<any>;
    week: any;
    weeks: any;
    wIndex: any;
    needsHighlightYellow: Function;
    needsThickBorder: Function;
    viewSettings: any;
    classInstructor: Function;
    getClasses: Function;
    noteText: Function;
    isEditable: Function;
    saveNotes: Function;
    noteIndex: Array<any>;
    onClick: Function;
  };
}

const TableInstructor = (props: TableInstructorProps) => {
  return (
    <tr key={'w_' + props.passed.wIndex} className={(props.passed.week.year) ? 'year-header' : '' + (props.passed.week.past) ? ' past' : '' + (props.passed.needsThickBorder(props.passed.wIndex, props.passed.weeks)) ? ' thick-border' : '' + props.passed.needsHighlightYellow(props.passed.wIndex, props.passed.weeks) ? ' highlight-yellow' : ''}>
      {props.passed.week.year ? (
        <td colSpan={props.passed.instructors.length + 1}>
          {props.passed.week.year}
        </td>
      ) : ([
        (
          <td key={0} className="date-cell">
            <div onClick={(event: any) => props.passed.viewSettings.toggleRowExpansion(props.passed.wIndex, event)}>
              <div>
                {
                  props.passed.week.weekStart ?
                    (
                      <div>{new Date(props.passed.week.weekStart).getMonth() + '/' + new Date(props.passed.week.weekStart).getDate() + ' - ' + new Date(props.passed.week.weekEnd).getMonth() + '/' + new Date(props.passed.week.weekEnd).getDate()}</div>
                    ) : 'No date'
                }
              </div>
            </div>
            {
              !props.passed.week.weekStart || <Notes onChange={(text: any) => {
                props.passed.saveNotes(props.passed.week.key, null, props.passed.noteIndex[props.passed.week.key], text);
              }} />
            }
          </td>
        ),
        ...props.passed.instructors.map((i: Assignee, iIndex: any) =>
          (
            <EngagementSquare
              key={'esq_' + iIndex}
              passed={{
                index: iIndex,
                assignee: i,
                instructors: props.passed.instructors,
                week: props.passed.week,
                getClasses: props.passed.getClasses,
                viewSettings: props.passed.viewSettings,
                classInstructor: props.passed.classInstructor,
                noteText: props.passed.noteText,
                isEditable: props.passed.isEditable,
                saveNotes: props.passed.saveNotes,
                noteIndex: props.passed.noteIndex,
                onClick: props.passed.onClick,
                wIndex: props.passed.wIndex
              }}
            />
          )
        ),
        (
          <td key={2}>
            <div style={{ width: 'calc(100vw - (80px + ' + props.passed.instructors.length + ' * 300px))' }} />
          </td>
          )])
      }
    </tr>
        );
};

export default TableInstructor;
