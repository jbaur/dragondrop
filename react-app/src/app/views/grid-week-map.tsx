import * as React from 'react';

import TableInstructor from './grid-table-instructors';

interface WeekMapProps {
  passed: {
    weeks: Array<any>;
    instructors: Array<any>;
    noteIndex: Array<any>;
    noteText: any;
    viewSettings: any;
    saveNotes: Function;
    getClasses: Function;
    isEditable: Function;
    classInstructor: Function;
    toggleRowExpansion: Function;
    needsHighlightYellow: Function;
    needsThickBorder: Function;
  };
}

const WeekMap = (props: WeekMapProps) => {
  return (
    <tbody>
      {/* Maps out weeks to TableIntructor */
        props.passed.weeks.map((week: any, wIndex: number) => {
          return (
            <TableInstructor
              key={'i_' + wIndex}
              passed={{
                instructors: props.passed.instructors,
                onClick: props.passed.toggleRowExpansion,
                week: week,
                weeks: props.passed.weeks,
                needsHighlightYellow: props.passed.needsHighlightYellow,
                needsThickBorder: props.passed.needsThickBorder,
                wIndex: wIndex,
                viewSettings: props.passed.viewSettings,
                classInstructor: props.passed.classInstructor,
                getClasses: props.passed.getClasses,
                noteText: props.passed.noteText,
                isEditable: props.passed.isEditable,
                saveNotes: props.passed.saveNotes,
                noteIndex: props.passed.noteIndex,
              }}
            />
          );
        })
      }
      <tr>
        <td colSpan={props.passed.instructors.length + 1} style={{ minWidth: props.passed.instructors.length * 100 + 90 + 'px' }}>
          <div className="pull-left">&copy; 2016 <a href="http://oasisdigital.com">Oasis Digital Solutions Inc.</a></div>
          <div className="pull-right" style={{ marginRight: '15px' }}>build $root.build</div>
        </td>
      </tr>
    </tbody>
  );
};

export default WeekMap;