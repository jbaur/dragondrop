import * as React from 'react';
import InstructorCard from './grid-instructor-card';

interface MapInstructorsProps {
  instructors: any;
}

const MapInstructors = (props: MapInstructorsProps) => {
  return (
    <tr>
      <th key={0} className="date-cell">Week</th>
      {
        props.instructors.map((i: any, index: number) => {
          return (
              <InstructorCard key={'ic' + index} passed={{...props, i, index}}/>
          );
        })
      }
      <th key={2}><div style={{ width: 'calc(100vw - (80px + ' + props.instructors.length + ' * 300px))' }} /></th>
    </tr>
  );
};

export default MapInstructors;