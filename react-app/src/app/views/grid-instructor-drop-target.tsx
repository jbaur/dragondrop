import * as React from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { DropTarget } from 'react-dnd';

import {  dragTypes } from '../data-types';
import { moveInstructor } from '../../stores/assignees-state/store';

class InstructorDropTarget extends React.Component<any, any> {
    render() {
        const { connectDropTarget } = this.props;

        return connectDropTarget (
            <div style={{height: '42px', width: '20px', display: 'table-cell'}} />
        )
    }
}

const classTarget = {
  canDrop(props: any, monitor: any) {
    // const item = monitor.getItem();
    return true;
  },

  drop(props: any, monitor: any, component: any) {
    props.moveInstructor({'old': monitor.getItem().passed.index, 'new': component.props.passed.index});
  }
};

/**
 * Specifies which props to inject into your component.
 */
function collect(connect: any, monitor: any) {
  return {
    // Call this function inside render()
    // to let React DnD handle the drag events:
    connectDropTarget: connect.dropTarget(),
    // You can ask the monitor about the current drag state:
    isOver: monitor.isOver(),
    isOverCurrent: monitor.isOver({ shallow: true }),
    canDrop: monitor.canDrop(),
    itemType: monitor.getItemType()
  };
}

// Maps the states to props on NavMenu
const mapStateToProps = (state: any, ownProps: any) => {
  return {
    ...ownProps
  };
};

// Maps dispatch functions to NavMenu
const mapDispatchToProps = (dispatch: Function) => {
  return {
      moveInstructor: (payload: any) => dispatch(moveInstructor(payload))
  };
};

// Provides wrapper for NavMenu, and maps the given props from mapStateToProps and mapDispatchToProps

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    DropTarget(dragTypes.INSTRUCTOR, classTarget, collect)
)(InstructorDropTarget)
