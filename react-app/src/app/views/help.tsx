import * as React from 'react';
import { Glyphicon, Row, Col } from 'react-bootstrap';

const Help = () => {
  return (
    <div id="legend-container" style={{ maxWidth: '650px' }}>
      <h3>Help</h3>
      <Row>
        <Col xs={4} sm={3}>
          <label>Summary</label>
          <div className="img-container"><img src="img/legend_summary.png" /></div>
        </Col>
        <Col xs={8} sm={5}>
          <label style={{ marginBottom: '20px' }}>Private</label>
          <div className="img-container"><img src="img/legend_sales.png" /></div>
          <label style={{ marginTop: '20px' }}>Detailed</label>
          <div className="img-container"><img src="img/legend_detail.png" /></div>
        </Col>
        <Col xs={12} sm={4}>
          <label>Public/Private</label>
          <div className="legend public">
            Public
          </div>
          <div className="legend">
            Private
          </div>
          <label>Status</label>
          <div className="legend public discussing-dates">
            Discussing dates
          </div>
          <div className="legend public payment-pending">
            Pending payment
          </div>
          <div className="legend public booked">
            Booked
          </div>
          <div className="legend public completed">
            Completed
          </div>
          <label>Travel</label>
          <div className="legend-text travel-icon">
            <Glyphicon glyph="send" /> Booking Needed
          </div>
          <div className="legend-text travel-icon">
            <Glyphicon glyph="plane" /> Booked
          </div>
          <div className="legend-text travel-icon">
            <Glyphicon glyph="flash" /> Online Instructor
          </div>
          <label>Sales</label>
          <div className="legend-text">
            <span className="sales-border" style={{ margin: '0 2px' }}>
              <span className="sales-inner sales-on-sale" />
            </span> On Sale
          </div>
          <div className="legend-text">
            <span className="sales-border" style={{ margin: '0 2px' }}>
              <span className="sales-inner sales-almost-sold-out" />
            </span> Almost Sold Out
          </div>
          <div className="legend-text">
            <span className="sales-border" style={{ margin: '0 2px' }}>
              <span className="sales-inner sales-sold-out" />
            </span> Sold Out
          </div>
          <div className="legend-text">
            <Glyphicon style={{ fontSize: '10px', color: '#888888' }} glyph="pencil" /> Draft
          </div>
          <div className="legend-text">
            <Glyphicon style={{ fontSize: '10px', color: '#888888' }} glyph="eye-close" /> Hidden
          </div>
        </Col>
      </Row>
    </div>
  );
};

export default Help;
