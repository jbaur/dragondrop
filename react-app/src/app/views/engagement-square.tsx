import * as React from 'react';
import { DropTarget } from 'react-dnd';
import { connect } from 'react-redux';
import { compose } from 'redux';

import { Assignee, dragTypes } from '../data-types';
import EngagementGridClass from './engagement-grid-class';
import Notes from './notes';
import { reassignClass, classReassignment } from './../../stores/data-state/store';

interface EngagementSquareProps {
  [key: string]: any;
  passed: {
    index: number;
    assignee: Assignee;
    instructors: Array<any>;
    week: any;
    viewSettings: any;
    classInstructor: Function;
    getClasses: Function;
    noteText: Function;
    isEditable: Function;
    saveNotes: Function;
    noteIndex: Array<any>;
    onClick: Function;
    wIndex: any;
  };
}

class EngagementSquare extends React.Component<EngagementSquareProps, any> {
  render() {
    const { connectDropTarget } = this.props;

    return connectDropTarget(
      <td style={{ minWidth: '100px', width: (96 / this.props.passed.instructors.length) + '%' }}>
        {
          this.props.passed.getClasses(this.props.passed.week, this.props.passed.assignee).map((c: any, cIndex: any) => {
            return (
              <EngagementGridClass onClick={(event: any) => this.props.passed.onClick(this.props.passed.wIndex, event)} key={'c_' + this.props.passed.wIndex + '_' + this.props.passed.index + '_' + cIndex} classInfo={c} instructor={this.props.passed.classInstructor(c, this.props.passed.assignee)} detailedView={this.props.passed.viewSettings.isExpanded(this.props.passed.wIndex)} />
            );
          })
        }
        {
          !this.props.passed.week.weekStart || <Notes
            initialext={this.props.passed.noteText(this.props.passed.week, this.props.passed.assignee)}
            editable={this.props.passed.isEditable(this.props.passed.week)}
            onChange={(text: any) => {
              this.props.passed.saveNotes(this.props.passed.week.key, this.props.passed.noteIndex[this.props.passed.week.key], text);
            }} />
        }
      </td>
    );
  }
};

const classTarget = {
  canDrop(props: any, monitor: any) {
    return props.passed.getClasses(props.passed.week, props.passed.assignee).length === 0;
  },

  drop(props: any, monitor: any, component: any) {
    const item = monitor.getItem();
    props.reassignClass({
      classInfo: item.classInfo,
      oldAssignee: item.instructor,
      newAssignee: component.props.passed.assignee
    });
  }
};

/**
 * Specifies which props to inject into your component.
 */
function collect(connect: any, monitor: any) {
  return {
    // Call this function inside render()
    // to let React DnD handle the drag events:
    connectDropTarget: connect.dropTarget(),
    // You can ask the monitor about the current drag state:
    isOver: monitor.isOver(),
    isOverCurrent: monitor.isOver({ shallow: true }),
    canDrop: monitor.canDrop(),
    itemType: monitor.getItemType()
  };
}

const mapStateToProps = (state: any, ownProps: any) => {
  return {
    ...ownProps
  };
};

const mapDispatchToProps = (dispatch: Function) => {
  return {
    reassignClass: (payload: classReassignment) => dispatch(reassignClass(payload))
  };
};

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  DropTarget(dragTypes.CLASS, classTarget, collect)
)(EngagementSquare);
