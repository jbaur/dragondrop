import * as React from 'react';

interface ErrorProps {
  error: any;
}

// Returns proper error given the props sent
const errorHandle = (props: ErrorProps) => {
  if (props.error.status === 401 && props.error.data) {
    return (
      <div>
        {props.error.data}
      </div>
    );
  } else if (props.error.status !== 401) {
    return (
      <div>
        <h4>Unable to load data</h4>
        <pre>{props.error}</pre>
      </div>
    );
  }
  return null;
};

const Error = (props: ErrorProps) => {
  return (
    <div className="alert alert-danger" style={{ margin: '15px' }}>
      {errorHandle(props)};
    </div>
  );
};

export default Error;