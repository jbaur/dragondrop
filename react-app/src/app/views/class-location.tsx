import * as React from 'react';
import SalesIndicator from './sales-indicator';

import { Class } from '../data-types';

interface CLProps {
  classInfo: Class;
  detailedView?: any;
}

export default class ClassLocation extends React.Component<CLProps, any> {
  constructor(props: CLProps) {
    super(props);
    this.state = {
      rows: []
    };
  }

  // Used to setstate after component mount to prevent errors
  componentDidMount() {
    let rows = [];

    if (this.props.classInfo) {
      if (this.props.classInfo.classLocationRegion) {
        rows.push({
          label: this.props.classInfo.classLocationRegion,
          sales: this.props.classInfo.salesState
        });
      }

      if (this.props.classInfo.online) {
        rows.push({
          label: 'Online',
          sales: this.props.classInfo.salesStateOnline
        });
      }
    }

    this.setState({
      rows: rows
    });
  }

// Ensures correct layout is returned based on whether detailed view is true
  detailedView() {
    if (this.props.detailedView) {
      return (
        <div style={{ height: (this.state.rows.length * 16) + 'px', position: 'relative' }}>
          {
            this.state.rows.map((row: any, i: number) => {
              return (
                <span key={'r_' + i} style={{ position: 'absolute', left: 0, display: 'inline-block', whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis', width: '100%', top: (i * 16) + 'px' }}>
                  {(this.props.classInfo.public) ? (<SalesIndicator sales={row.sales} />) : ''}
                  {row.label}
                </span>
              );
            })
          }
        </div>
      );
    } else {
      return (
        <div>
          {
            this.state.rows.map((row: any, i: number) => {
              return (
                <span key={'r_2_' + i} style={{ display: 'inline-block' }}>
                  {(this.props.classInfo.public) ? <SalesIndicator sales={row.sales} /> : ''}
                  {row.label}
                </span>
              );
            })
          }
        </div>
      );
    }
  }

  render() {
    return this.detailedView();
  }
}
