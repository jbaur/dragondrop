import * as React from 'react';
import { DragSource } from 'react-dnd';

import InstructorDropTarget from './grid-instructor-drop-target'
import { dragTypes } from '../data-types';

class InstructorCard extends React.Component<any, any> {
    render() {
        const { connectDragSource } = this.props;

        return connectDragSource (
            <th key={'iHead_' + this.props.passed.index} className="text-center" style={{ minWidth: '100px', width: (96 / this.props.passed.instructors.length) + '%' }}>
            <InstructorDropTarget key={'dt' + this.props.passed.index} passed={{...this.props.passed}}/>
            <span style={{display: 'table-cell'}}>{this.props.passed.i.name}</span></th>
        )
    }
}

const classSource = { 
  beginDrag(props: any) { 
    return props; 
  } 
} 
 
function collect(connect: any, monitor: any) { 
  return { 
    connectDragSource: connect.dragSource(), 
    isDragging: monitor.isDragging() 
  } 
} 

export default DragSource(dragTypes.INSTRUCTOR, classSource, collect)(InstructorCard);
