import * as React from 'react';
import { connect } from 'react-redux';
import * as moment from 'moment';
import * as _ from 'lodash';
import { FormControl } from 'react-bootstrap';

import { populateNotes } from '../../stores/notes-state/store';

const MAX_NOTES = 200;

class Audit extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      visibleNotes: [],
      filter: {}
    };
  }

  // Used to update search everytime the store updates
  componentWillReceiveProps(nextProps: any) {
    if (!this.props.notesStorage.allNotes && nextProps.notesStorage.allNotes) {
      this.search();
    }
  }

  search = (f?: any) => {
    f = f || this.state.filter;
    this.setState({
      visibleNotes: _(this.props.notesStorage.allNotes)
        .sortBy('asOf')
        .filter((note: any) => {
          return (!f.enteredBy || note.enteredBy.indexOf(f.enteredBy) >= 0) &&
            (!f.instructor || note.instructor.indexOf(f.instructor) >= 0) &&
            (!f.notes || note.notes.indexOf(f.notes) >= 0);
        })
        .value()
        .slice(-MAX_NOTES),
      filter: f
    });
  }

  weekEnd = (weekStart: any) => {
    return moment(weekStart).add(6, 'days').format();
  }

  updateFilter = (event: any) => {
    let f = {...this.state.filter};
    f[event.target.name] = event.target.value;
  }

  render() {
    return (
      <div id="audit-log" className="container-fluid"> {/* manage-layout */}
        <div className="panel panel-default" style={{ marginTop: '15px' }}>
          <div className="panel-body">
            <h4>Search audit log</h4>
            <form className="form-inline" style={{ margin: '10px 0' }}>
              <div className="form-group">
                <label>Edited by: </label>
                <FormControl onChange={this.updateFilter} type="text" bsSize="sm"/>
              </div>
              <div className="form-group" style={{ margin: '0 10px' }}>
                <label>Instructor: </label>
                <FormControl onChange={this.updateFilter} type="text" bsSize="sm" />
              </div>
              <div className="form-group">
                <label>Notes: </label>
                <FormControl onChange={this.updateFilter} type="text" bsSize="sm" />
              </div>
            </form>
          </div>
        </div>
        <table className="table">
          <thead>
            <th className="date-time-cell">Edited at</th>
            <th style={{ width: '15%' }}>Edited by</th>
            <th className="date-range-cell">Week</th>
            <th style={{ width: '15%' }}>Instructor</th>
            <th>Notes</th>
          </thead>
          <tbody>
            {
              this.state.visibleNotes.map((note: any) => {
                return (
                  <tr>
                    <td className="date-time-cell">{moment(note.asOf).format('M/d/yy h:mm a')}</td>
                    <td style={{ width: '15%' }}>{note.enteredBy}</td>
                    <td className="date-range-cell">{moment(note.week).format('M/d/yy')} - {moment(this.weekEnd(note.week)).format('M/d/yy')}</td>
                    <td style={{ width: '15%' }}>{note.instructor}</td>
                    <td>{note.notes}</td>
                  </tr>
                );
              })
            }
          </tbody>
        </table>
      </div>
    );
  }
}

const mapStateToProps = (state: any, ownProps: any) => {
  return {
    ...ownProps,
    notesStorage: state.notesStorage
  };
};

const mapDispatchToProps = (dispatch: Function) => {
  return {
    populateNotes: (data: any) => dispatch(populateNotes(data))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Audit);
