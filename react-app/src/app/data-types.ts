export interface Week {
  year?: string;
  weekStart?: string;
  weekEnd?: string;
  key?: string;
  past?: boolean;
}

export interface Assignee {
  key: string;
  name: string;
  email: string;
  hidden: boolean;
  myself: boolean;
  id: string;
  groups: string[];
  travel: string;
}

export interface Class {
  key: string;
  url: string;
  summary: string;
  type: string;
  status: string;
  classInterest: any;
  workshopInterest: any;
  hasDate: boolean;
  salesState: string;
  salesStateOnline: string;
  startDateString: string;
  endDateString: string;
  duration: number;
  customerName: string;
  booked: boolean;
  timeZone: string;
  keyContact: string;
  datesOffered: string;
  leadSources: any;
  ticketsSeats: string;
  invoiced: any;
  instructorNeeds: any;
  trainingDodList: any;
  classLocationRegion: string;
  classType: any;
  trainingQuestion: any;
  startDate: any;
  endDate: any;
  classDates: string;
  public: any;
  angular2: boolean;
  instructors: Assignee[];
  online: boolean;
}

export interface ClassInfo {

}

export interface Data {
  Assignee_displayName: string;
  Assignee_emailAddress: string;
  Assignee_key: string;
  Assignee_username: string;
  Class_Duration: number;
  Class_Location__Region_: string;
  Class_Type_0: string;
  Class__Interest__0: string;
  Customer_Name: string;
  Date_Held: string;
  Dates_Offered: string;
  Instructor_Need_0: string;
  Issue_Type: string;
  Issue_URL: string;
  Key: string;
  Key_Contact: string;
  Primary_Instructor_Travel_child: string;
  Primary_Instructor_Travel_value: string;
  Primary_Instructor_displayName: string;
  Primary_Instructor_emailAddress: string;
  Primary_Instructor_key: string;
  Primary_Instructor_username: string;
  Second_Instructor_Travel_child: string;
  Second_Instructor_Travel_value: string;
  Second_Instructor_displayName: string;
  Second_Instructor_emailAddress: string;
  Second_Instructor_key: string;
  Second_Instructor_username: string;
  Status: string;
  Summary: string;
  Third_Instructor_Travel_child: object;
  Third_Instructor_Travel_value: string;
  Tickets_Seats: string;
  Time_Zone: string;
}

export const dragTypes = {
  CLASS: 'class',
  INSTRUCTOR: 'instructor'
};
