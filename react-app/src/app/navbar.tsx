import * as React from 'react';
import { DropdownButton, MenuItem, FormControl, FormGroup, NavItem, Nav, Navbar, Glyphicon, Button } from 'react-bootstrap';
import { connect } from 'react-redux';

import { setView, addGroupItems, toggleGroupItem } from '../stores/filter-state/store';
import RemoveInstructor from './remove-instructor';
import { addNewGroup } from '../stores';

class NavMenu extends React.Component<any, any> {
  newGroup: any;
  constructor(props: any) {
    super(props);
    this.state = {
      groups: [],
      type: 3,
      assigneeGroups: [],
      disabled: false,
      checked: [],
      groupSelectorToggled: false
    };
  }
  // Retrieving assignee groups from store when the component mounts.
  // ComponentWillMount() is one of the react digest cycle functions
  componentWillMount() {
    this.setState({ assigneeGroups: this.props.assignees.groups });
  }

  // Handles a change in the type (public/private) filter using the setView function from the function-state store
  handleTypeChange = (event: any) => {
    var typeVal = event.target.value * 1;
    this.props.setView(typeVal);
  }
  // Handles a change in each group filter option, uses the function toggleGroupItem from filter-state store
  handleGroupChange = (key: string) => {
    this.props.toggleGroupItem(key);
  }

  // Toggles what can be selected on group dropdown
  toggleDropdown = (isOpen: boolean, event?: any, source?: any) => {
    if (source) {
      switch (source.source) {
        case 'select':
          break;
        default:
          this.setState({ groupSelectorToggled: !this.state.groupSelectorToggled });
          break;
      }
    }
  }

  // Maps the group options from the store to the drop down options
  makeGroupOptions = () => {
    let iterator = 0;
    return [
      ...Object.keys(this.props.filter.groups).map((key: string) => {
        iterator++;
        const group = this.props.filter.groups[key];
        var check = !!this.props.filter.groups[key].enabled;
        return (
          <MenuItem href="" key={iterator} onClick={(event: any) => { event.stopPropagation(); this.handleGroupChange(key); }}>
            <Glyphicon glyph={check ? 'ok' : ''} /> {group.name}
          </MenuItem>
        );
      })
    ];
  }

  saveNewGroup = () => {
    this.props.addNewGroup(this.newGroup.value);
  }

  render() {
    let filtered = Object.keys(this.props.filter.groups).filter((key: string) => {
      return this.props.filter.groups[key].enabled;
    });
    let name = this.props.filter.groups[filtered[0]].name;
    let title = (filtered.length > 1) ? name.substring(0, 5) + '...' : name;
    return (
      <Navbar fluid={true} collapseOnSelect={true}>
        <Navbar.Header>
          <Navbar.Brand><a>Dev</a></Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav pullRight={false}>
            <NavItem onClick={() => this.props.history.push('/grid')}
              className={(this.props.location.pathname === '/grid' ? 'active' : '')}
            >
              Grid
            </NavItem>
            <NavItem onClick={() => this.props.history.push('/audit')}
              className={(this.props.location.pathname === '/audit' ? 'active' : '')}
            >
              Audit
            </NavItem>
            <NavItem onClick={() => this.props.history.push('/help')}
              className={(this.props.location.pathname === '/help' ? 'active' : '')}
            >
              Help
            </NavItem>
          </Nav>
          <Navbar.Form className="nav-form">
            <FormGroup>
              <FormControl bsSize="small" componentClass="select" onChange={this.handleTypeChange} name="type">
                <option value={3}>Public / Private</option>
                <option value={2}>Private</option>
                <option value={1}>Public</option>
              </FormControl>
            </FormGroup>
            <FormGroup style={{ marginLeft: '5px' }}>
              <DropdownButton bsSize="small" open={this.state.groupSelectorToggled} onToggle={this.toggleDropdown} title={title} id="dropdown-size-large">
                {this.makeGroupOptions()}
              </DropdownButton>
            </FormGroup>
            <FormGroup style={{ float: 'right' }}>
              <div>{(this.props.assignees.identity.User.Email) ? this.props.assignees.identity.User.Email + ' ' : 'Loading...'}</div>
              <Button href="/switch-account" bsSize="xsmall">Switch Account</Button>
            </FormGroup>
            <FormGroup style={{ float: 'right', marginRight: '40px' }}>
              <FormControl inputRef={(ref) => { this.newGroup = ref }} placeholder="New Group" />
              <Button onClick={this.saveNewGroup}>Save</Button>
            </FormGroup>
            <FormGroup style={{ float: 'right' }}>
              <RemoveInstructor />
            </FormGroup>
          </Navbar.Form>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}
// Maps the states to props on NavMenu
const mapStateToProps = (state: any, ownProps: {}) => {
  return {
    ...ownProps,
    filter: state.filter,
    data: state.data,
    assignees: state.assignees
  };
};

// Maps dispatch functions to NavMenu
const mapDispatchToProps = (dispatch: Function) => {
  return {
    setView: (value: number) => dispatch(setView(value)),
    addGroupItems: (assigneeGroups: Array<string>) => dispatch(addGroupItems(assigneeGroups)),
    toggleGroupItem: (key: string) => dispatch(toggleGroupItem(key)),
    addNewGroup: (name: string) => dispatch(addNewGroup(name))
  };
};

// Provides wrapper for NavMenu, and maps the given props from mapStateToProps and mapDispatchToProps
export default connect(mapStateToProps, mapDispatchToProps)(NavMenu);
