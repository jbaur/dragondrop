(function (angular, _) {
  'use strict';

  const module = angular.module('assigneeGroupSelector', [
    'angularjs-dropdown-multiselect'
  ]);

  module.component('assigneeGroupSelector', {
    templateUrl: 'assigneeGroupSelector.html',
    bindings: {
      groups: '<',
      initialGroup: '<',
      onChange: '&'
    },
    controller: function () {
      this.availableGroups = [];
      this.selectedGroups = [];

      this.dropdownSettings = {
        displayProp: 'name',
        showCheckAll: false,
        showUncheckAll: false,
        smartButtonMaxItems: 1
      };
      this.eventHandlers = {
        onItemSelect: (item) => this._onGroupSelect(item.id),
        onItemDeselect: (item) => this._onGroupDeselect(item.id)
      }

      this._onGroupSelect = function _onGroupSelect(groupId) {
        if (groupId === '$$just-me') {
          this.selectedGroups = _.filter(this.availableGroups, { id: '$$just-me' })
        } else if (groupId === '$$everyone' || this._allGroupsSelected()) {
          this.selectedGroups = _.reject(this.availableGroups, { id: '$$just-me' });
        } else {
          this.selectedGroups = _.reject(this.selectedGroups, { id: '$$just-me' });
        }
        this._fireOnChange();
      };

      this._onGroupDeselect = function _onGroupDeselect(groupId) {
        if (groupId === '$$everyone' || this.selectedGroups.length === 0) {
          this.selectedGroups = _.filter(this.availableGroups, { id: '$$just-me' });
        } else {
          this.selectedGroups = _.reject(this.selectedGroups, { id: '$$everyone' });
        }
        this._fireOnChange();
      }

      this.$onInit = function onInit() {
        this.availableGroups = [
          { id: '$$everyone', name: 'Everyone' },
          { id: '$$just-me', name: 'Just me' }
        ].concat(this.groups);

        this._onGroupSelect(this.initialGroup);
      };

      this._allGroupsSelected = function () {
        return _(this.availableGroups)
          .filter(g => g.id !== '$$everyone' && g.id !== '$$just-me')
          .every(g => !!_.find(this.selectedGroups, { id: g.id }));
      }

      this._fireOnChange = function () {
        const hydrated = _.filter(this.availableGroups, group => !!_.find(this.selectedGroups, { id: group.id }));
        this.onChange({ selectedGroups: hydrated });
      }
    }
  });

}(angular, _));
