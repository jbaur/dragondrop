(function (angular, _) {
  'use strict';

  angular.module('engagementGridFilters', ['responsiveness'])
    .component('engagementGridFilters', {
      bindings: {
        engagements: '<',
        onChange: '&',
        viewSettings: '='
      },
      templateUrl: 'engagementGridFilters.html',
      controller: function (responsiveness, engagementGridAssignees, notesStorage, dateUtil) {
        let $ctrl = this;
        let fourWeeksAgo = dateUtil.mWeekStart(new Date()).subtract(4, 'weeks');

        this.groups = engagementGridAssignees.getGroups();
        this.initialGroup = responsiveness.smallScreen() ? '$$just-me' : '$$everyone';

        this.filter = {
          fullHistory: false,
          publicPrivate: 'all',
          assigneeGroups: []
        };

        function getMatchingAssignees(groups) {
          const assignees = engagementGridAssignees.getAssignees();
          const groupIds = _.map(groups, g => g.id);
          let matchingAssignees;
          if (groupIds.indexOf('$$everyone') >= 0) {
            matchingAssignees = assignees;
          } else if (groupIds[0] === '$$just-me') {
            matchingAssignees = _.filter(assignees, { myself: true });
          } else {
            matchingAssignees = _.filter(assignees, a => _.intersection(a.groups, groupIds).length > 0);
          }
          return _.sortBy(matchingAssignees, [
            a => _(a.groups)
                .filter(groupId => groupIds.indexOf(groupId) >= 0)
                .map(groupId => _.find(groups, { id: groupId }).sortOrder)
                .min(),
            a => a.key
          ]);
        }

        function hasMatchingAssignee(assigneeEmails, c) {
          return _.some(c.instructors, i => assigneeEmails.indexOf(i.email) >= 0);
        }

        this.filterEngagements = function () {
          let groups = $ctrl.filter.assigneeGroups;
          let matchingAssignees = getMatchingAssignees(groups);
          let assigneeEmails = _.map(matchingAssignees, 'email');
          let matchingEngagements = _.filter($ctrl.engagements, function (c) {
            if (!$ctrl.filter.fullHistory) {
              if (c.hasDate && fourWeeksAgo.isAfter(c.startDate)) {
                return false;
              }
            }
            if ($ctrl.filter.publicPrivate === 'public' && !c.public) {
              return false;
            }
            if ($ctrl.filter.publicPrivate === 'private' && c.public) {
              return false;
            }
            if (!hasMatchingAssignee(assigneeEmails, c)) {
              return false;
            }
            return true;
          });
          let matchingNotes = _.filter(notesStorage.notes, function (n) {
            return $ctrl.filter.fullHistory || !fourWeeksAgo.isAfter(n.week);
          });

          $ctrl.onChange({
            engagements: matchingEngagements,
            notes: matchingNotes,
            assignees: matchingAssignees
          });
        };

        this.$onInit = function onInit() {
          this.filterEngagements();
        };

        this.viewSettingsChanged = function () {
          this.viewSettings.rowExpansion = [];
        };

        this.assigneeGroupsChanged = function assigneeGroupsChanged(selectedGroups) {
          this.filter.assigneeGroups = selectedGroups;
          this.filterEngagements();
        }
      }
    });

}(angular, _));
