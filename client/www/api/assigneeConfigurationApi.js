(function (angular) {
  'use strict';

  let module = angular.module('api.assigneeConfigurationApi', []);

  module.service('assigneeConfigurationApi', function ($http, $q) {
    let data = undefined;

    this.getConfiguration = function getConfiguration() {
      if (!data) {
        data = $http.get('/api/assignee-configuration').then(unwrapResponse);
      }
      return data;
    };

    this.save = function save(assigneesAndGroups) {
      return $http
        .post('/api/assignee-configuration', assigneesAndGroups)
        .then(unwrapResponse)
        .then(resp => this.data = $q.resolve(resp));
    };
  });

  function unwrapResponse(resp) {
    return resp.data;
  }
}(window.angular));
