(function (angular, _, moment) {
  'use strict';

  angular.module('engagementGrid', [])
    .component('engagementGrid', {
      templateUrl: 'engagementGrid.html',
      controller: function ($log, $window, $q, identity, assigneeConfigurationApi, engagementGridAssignees,
        engagementData, notesStorage, dateUtil) {
        const $ctrl = this;

        function dataLoadFailed(error) {
          $ctrl.status = 'ERROR';
          $ctrl.error = error;
          $log.error('Unable to load data', error);
        }

        function populate(_identity, _assigneeConfiguration, data) {
          engagementGridAssignees.populate(_identity, _assigneeConfiguration, data.classes);
          $ctrl.identity = _identity;
          $ctrl.data = data;
        }

        this.reloadView = function (_assignees, _engagements, notes) {
          function groupByInstructor(weekClasses) {
            let byInstructor = {};
            weekClasses.forEach(c =>
              c.instructors.forEach(i => {
                if (!byInstructor[i.key]) {
                  byInstructor[i.key] = [];
                }
                byInstructor[i.key].push(c);
              }));
            return byInstructor;
          }

          if (_engagements.length === 0 && notes.length === 0) {
            if ($ctrl.data.classes.length > 0 || notesStorage.notes.length > 0) {
              $ctrl.status = 'DATA_FILTERED';
            } else {
              $ctrl.status = 'NO_DATA';
            }
            return;
          } else {
            $ctrl.status = 'DATA_PRESENT';
          }

          let weekStart;
          let lastWeek;
          let weekDates;
          const byWeek = _(_engagements)
            .filter(c => c.hasDate)
            .sortBy(c => c.startDateString + '-' + c.endDateString)
            .groupBy(c => dateUtil.mWeekStart(c.startDate).format())
            .map((weekClasses, week) => [week, groupByInstructor(weekClasses)])
            .fromPairs()
            .value();

          const noDates = _(_engagements)
            .filter(c => !c.hasDate)
            .sortBy(['type', 'customerName'])
            .value();

          $ctrl.classes = {};
          Object.assign($ctrl.classes, byWeek);
          $ctrl.classes['nodate'] = groupByInstructor(noDates);

          $ctrl.weeks = [];

          weekDates = _.concat(Object.keys(byWeek), _.map(notes, 'week'));

          if (weekDates.length > 0) {
            lastWeek = moment(_.max(weekDates)).add(4, 'weeks');
            weekStart = moment(_.min(weekDates));
            while (!weekStart.isAfter(lastWeek)) {
              if ($ctrl.weeks.length === 0 ||
                weekStart.format('YYYY') !== moment(weekStart).subtract(7, 'days').format('YYYY')) {
                $ctrl.weeks.push({ year: weekStart.format('YYYY') });
              }
              $ctrl.weeks.push({
                weekStart: weekStart.format(),
                weekEnd: moment(weekStart).add(6, 'days').format(),
                key: moment(weekStart).format('YYYY-MM-DD'),
                past: moment(weekStart).add(5, 'days').isBefore(new Date())
              });
              weekStart = moment(weekStart).add(7, 'days');
            }
          }
          $ctrl.weeks.push({});

          $ctrl.instructors = _assignees;
          $ctrl.viewSettings.rowExpansion = [];

          $ctrl.noteIndex = _(notes)
            .map(function (n) {
              return [n.week + (n.instructor ? '-' + n.instructor : ''), n.notes];
            })
            .fromPairs()
            .value();
        };

        this.hasInstructor = function (classInfo, instructor) {
          return !!this.classInstructor(classInfo, instructor);
        };

        this.classInstructor = function (classInfo, instructor) {
          return _.find(classInfo.instructors, { key: instructor.key });
        };

        this.getClasses = function (week, instructor) {
          const classes = $ctrl.classes[week.weekStart || 'nodate'] || [];
          return classes[instructor.key] || [];
        };

        this.viewSettings = {
          detailedView: false,
          rowExpansion: [],
          isExpanded: function (row) {
            let vs = $ctrl.viewSettings;
            return typeof vs.rowExpansion[row] === 'boolean' ?
              vs.rowExpansion[row] : vs.detailedView;
          },
          toggleRowExpansion: function (row, $event) {
            if ($event.target.tagName !== 'A') {
              $ctrl.viewSettings.rowExpansion[row] = !$ctrl.viewSettings.isExpanded(row);
            }
          }
        };

        this.saveNotes = function (week, instructor, text) {
          notesStorage.save(week, instructor, text);
        };

        this.needsThickBorder = function (index) {
          return index > 0 &&
            (
              this.weeks[index - 1].past && !this.weeks[index].past ||
              this.weeks[index].year ||
              this.weeks[index - 1].year
            );
        };

        this.needsHighlightYellow = function (index) {
          return dateUtil.mWeekStart(moment().add(4, 'weeks'))
            .format() === this.weeks[index].weekStart;
        };

        this.isEditable = function(week) {
          return $ctrl.identity.Rights.EnterNotes && moment(week.weekStart).isAfter(dateUtil.mWeekStart(moment().subtract(5, 'weeks')));
        };

        this.noteText = function noteText(w, i) {
          return $ctrl.noteIndex[w.key + '-' + i.email] || '';
        };

        this.status = 'LOADING';
        $q.all([identity,
          assigneeConfigurationApi.getConfiguration(),
          engagementData.get(),
          notesStorage.populate()
        ])
          .then(results => populate(results[0], results[1], results[2]))
          .catch(dataLoadFailed);
      }
    })
    .directive('manageLayout', function ($document, $window) {
      return function (scope, el, attrs) {
        $document[0].body.style.overflow = 'hidden';
        // The scroll event binding, and setting window scroll position to 0, is needed to improve
        // hnadling of zoom events on mobile devices (Android at least). Otherwise pinch zoom and
        // keyboard show/hide may lead to the grid having odd size or position.
        let table = el.find('table')[0];
        let tbody = el.find('tbody')[0];
        let theadRow = el.find('thead').find('tr')[0];
        let jqWindow = angular.element($window);
        let resizeTimeout = null;
        function triggerResize() {
          if (!resizeTimeout) {
            resizeTimeout = setTimeout(onResize, 50);
          }
        }
        function onResize() {
          tbody.style.height = $window.innerHeight - table.offsetTop - tbody.offsetTop + 'px';
          $document[0].body.scrollTop = 0;
          resizeTimeout = null;
        }
        jqWindow.bind('resize', triggerResize);
        jqWindow.bind('scroll', triggerResize);
        scope.$watch(function () { return table.offsetTop; }, triggerResize);
        onResize();

        scope.$on('$destroy', function () {
          jqWindow.unbind('resize', triggerResize);
          jqWindow.unbind('scroll', triggerResize);
          $document[0].body.style.overflow = '';
        });

        tbody.addEventListener('scroll', () => {
          theadRow.style.marginLeft = `-${tbody.scrollLeft}px`;
        });
      };
    })
    .directive('scrollBottom', function () {
      return {
        scope: {
          scrollBottom: '='
        },
        link: function (scope, element) {
          scope.$watchCollection('scrollBottom', function (newValue) {
            if (newValue) {
              let el = angular.element(element)[0];
              el.scrollTop = el.scrollHeight;
            }
          });
        }
      };
    })
    .component('engagementGridClass', {
      templateUrl: 'engagementGridClass.html',
      bindings: {
        classInfo: '<',
        instructor: '<',
        detailedView: '<'
      },
      controller: function ($scope) {
        this.$onInit = function onInit() {
          Object.assign(this, this.classInfo);
          switch (this.status) {
            case 'Decide Date':
            case 'Discussing Dates':
              this.statusClass = 'discussing-dates';
              break;
            case 'Downpayment':
            case 'Invoiced (Billed)':
              this.statusClass = 'payment-pending';
              break;
            case 'Reserve Facility':
              this.statusClass = 'need-to-reserve';
              break;
            case 'Booked Class (Deposit)':
            case 'Booked Class (PO Only)':
            case 'Class Scheduled':
              this.statusClass = 'booked';
              break;
            case 'Class Completed':
            case 'Closed':
              this.statusClass = 'completed';
              break;
          }
          this.tooltip = '';
          if (this.customerName) {
            this.tooltip += 'Customer: ' + this.customerName + '\n';
          }
          this.tooltip += 'Status: ' + this.status;
          this.instructorPosition = this.instructor.key !== '(Unassigned)' &&
            _.findIndex(this.classInfo.instructors, { key: this.instructor.key });
          if (this.hasDate) {
            if (this.startDate.month() === this.endDate.month()) {
              this.classDates = this.startDate.format('M / D') + '-' + this.endDate.format('D');
            } else {
              this.classDates = this.startDate.format('M/D') + ' - ' + this.endDate.format('M/D');
            }
          } else {
            this.classDates = '?';
          }
          this.statusAbbr = this.status.replace(' Class', '');
        };
      }
    })
    .component('classLocation', {
      templateUrl: 'classLocation.html',
      bindings: {
        classInfo: '<',
        detailedView: '<'
      },
      controller: function () {
        this.rows = [];
        this.$onInit = function onInit() {
          if (!!this.classInfo.classLocationRegion) {
            this.rows.push({
              label: this.classInfo.classLocationRegion,
              sales: this.classInfo.salesState
            });
          }
          if (!!this.classInfo.online) {
            this.rows.push({ label: 'Online', sales: this.classInfo.salesStateOnline });
          }
        };
      }
    })
    .component('salesIndicator', {
      templateUrl: 'salesIndicator.html',
      bindings: {
        sales: '<'
      }
    });

}(angular, _, moment));
