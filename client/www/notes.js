(function (angular, _, moment) {
  'use strict';

  angular.module('notes', [])
    .service('notesStorage', function ($http, $q, $timeout) {
      const notesStorage = this;

      this.notes = {};
      this.allNotes = [];

      this.populate = function () {
        return $http.get('/api/notes').then(function (resp) {
          notesStorage.allNotes = resp.data;

          notesStorage.notes = _(resp.data)
            .sortBy('asOf')
            .reverse()
            .uniqBy(function (v) {
              return v.week + (v.instructor ? '-' + v.instructor : '');
            })
            .value();
        });
      };

      this.save = function (week, instructor, notes) {
        let current = _.find(this.notes, { week: week, instructor: instructor }) || '';
        if (current !== notes) {
          $http.post('/api/notes', {
            week: week,
            instructor: instructor,
            notes: notes
          });
          $timeout(notesStorage.populate, 5000);
        }
      };
    })
    .component('notes', {
      templateUrl: 'notes.html',
      bindings: {
        initialText: '<',
        editable: '<',
        onChange: '&'
      },
      controller: function () {
        this.$onInit = function onInit() {
          this.text = this.initialText;
        };

        this.saveChanges = function () {
          this.onChange({ text: this.text });
        };
      },
    })
    .directive('cellWithNotes', function () {
      function placeCaretAtTheEnd(el) {
        if (typeof window.getSelection !== 'undefined' &&
          typeof document.createRange !== 'undefined') {
          let range = document.createRange();
          range.selectNodeContents(el);
          range.collapse(false);
          let sel = window.getSelection();
          sel.removeAllRanges();
          sel.addRange(range);
        } else if (typeof document.body.createTextRange !== 'undefined') {
          let textRange = document.body.createTextRange();
          textRange.moveToElementText(el);
          textRange.collapse(false);
          textRange.select();
        }
      }

      return function (scope, el, attr) {
        el.bind('click', () => {
          let spans = el.find('span');
          for (let i = 0; i < spans.length; i++) {
            if (spans[i].hasAttribute('contenteditable')) {
              spans[i].focus();
              placeCaretAtTheEnd(spans[i]);
              break;
            }
          }
        });
      };
    })
    .directive('contenteditable', function () {
      return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
          function read() {
            ngModel.$setViewValue(element.html());
          }

          ngModel.$render = function () {
            element.html(ngModel.$viewValue || '');
          };

          element.bind('blur keyup change', function () {
            scope.$apply(read);
          });
        }
      };
    });

}(angular, _, moment));
