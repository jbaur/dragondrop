(function (angular) {
  'use strict';

  let module = angular.module('config.assigneeGroupConfigState', [
    'api.assigneeConfigurationApi'
  ]);

  module.service('assigneeGroupConfigState', function (assigneeConfigurationApi) {
    this.dataLoadingStatus = 'loading';
    this.saveInProgress = false;
    this.saveError = undefined;

    this.save = function save() {
      this.saveError = undefined;
      this.saveInProgress = true;
      const groups = _.map(this.groups, g => {
        if (g.id.indexOf('$$temp') == 0) {
          return Object.assign({}, g, { id: undefined, tempId: g.id });
        } else {
          return g;
        }
      });
      const assignees = _.map(this.assignees, a => {
        const hasTempGroups = _.some(a.groups, g => g.indexOf("$$temp") == 0);
        if (hasTempGroups) {
          return Object.assign({}, a, {
            groups: _.filter(a.groups, g => g.indexOf("$$temp") == -1),
            tempGroups: _.filter(a.groups, g => g.indexOf("$$temp") == 0),
          });
        } else {
          return a;
        }
      });
      assigneeConfigurationApi
        .save({ groups, assignees })
        .then((resp) => {
          this.groups = resp.groups;
          this.assignees = resp.assignees;
        })
        .catch((error) => this.saveError = error)
        .finally(() => this.saveInProgress = false);
    };

    assigneeConfigurationApi.getConfiguration()
      .then(({ groups, assignees }) => {
        this.groups = groups;
        this.assignees = assignees;
        this.dataLoadingStatus = 'ok';
      })
      .catch(err => {
        this.loadError = err;
        this.dataLoadingStatus = 'error';
      });
  });

  function unwrapResponse(resp) {
    return resp.data;
  }
}(window.angular));
