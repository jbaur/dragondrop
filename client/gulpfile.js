// This simple example Gulpfile bundles all of the JavaScript,
// but brings the other assets over separately.
/* eslint no-undef: 0 */
/* eslint strict: 0 */

const fs = require('fs'),
  gulp = require('gulp'),
  concat = require('gulp-concat'),
  uglify = require('gulp-uglify'),
  annotate = require('gulp-ng-annotate'),
  babel = require('gulp-babel'),
  templateCache = require('gulp-angular-templatecache'),
  git = require('git-rev-sync');

const DIST = '../static';

gulp.task('templates', () => {
  return gulp.src('www/**/*.html')
    .pipe(templateCache('templates.js', { 'module': 'app' }))
    .pipe(gulp.dest('build'));
});

gulp.task('buildInfo', (cb) => {
  fs.writeFile('build/buildInfo.js', `window.build = '${git.long()}';`, cb);
});

gulp.task('js', ['templates', 'buildInfo'], () => {
  gulp.src(['build/buildInfo.js', 'www/**/*.js', 'build/templates.js'])
    .pipe(babel({
      presets: ['es2015']
    }))
    .pipe(annotate())
    .pipe(concat('bundle.js'))
    .pipe(uglify())
    .pipe(gulp.dest(DIST));
});

gulp.task('copy', () => {
  gulp.src(['www/**/*.jpg',
    'www/**/*.png',
    'www/**/*.json',
    'www/**/*.css',
    'www/index.html'])
    .pipe(gulp.dest(DIST));
});

gulp.task('build', ['js', 'copy']);

gulp.task('default', ['build']);

gulp.task('watch', function () {
  gulp.watch('www/**', ['build']);
});
 