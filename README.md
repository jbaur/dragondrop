# Engagement Grid

This application was originally created to show training classes of
Oasis Digital/Expium in a convenient, easy-to-grasp grid. The classes
were maintained in JIRA (as issues) and exposed to the web via our Query
Feed for JIRA add-on.

It has since expanded to be:

* More Generic
* Not about training specifically, as we use it for other engagements and planning

This application takes data from Query Feed and presents all the classes
in a single grid, with rows for weeks and columns for instructors. It
consists of two parts: Go server on AppEngine (hides data URL and
performs authorization), and client app in Angular.

## Access Control

Access control is driven by the PrincipalRights entities in Google Cloud
Datastore.

Each entity has an EmailRegexp property which determines what emails
(users) it affects. Only users who have at least one matching
PrincipalRights entity will be allowed to use the application. For
example, ".*@oasisdigital.com" grants access to all accounts in
oasisdigital.com domain and nothing else. Add "jim@gmail.com" or
"(jim|jill)@gmail.com" to add others.

The app also allows anonymous access, though it's mostly intended for
demo use. Anonymous users are matched with email equal "Anonymous". This
way anonymous access is granted using such EmailRegexp as "Anonymous" or
".*".

PrincipalRights also determine what the users can do in the application.

If user email matches more than one PrincipalRights entry, the
permission is granted if at least one of them grants it. For example,
suppose there is are entries with EmailRegexp=.*@oasisdigital.com and
EmailRegexp=joe.smith.*. User with email joe.smith@oasisdigital.com will
be have all the rights that are granted by any of these rows.

The rights are as follows:

* `SeeOwnClasses` - see classes and notes for the currently logged in
  user
* `SeeAllClasses` - see classes and notes for all users
* `EnterNotes` - create and edit notes in the grid
* `SeeFullHistory` - see full history of classes, users without
  this permission only see a few weeks
* `Admin` - access administration

## Running the App

Install Python 2.7, make sure it's in your PATH environment property.

Install Go, currently 1.6.

Install Google AppEngine SDK: https://cloud.google.com/appengine/downloads#Google_App_Engine_SDK_for_Go

### Note for Windows Users

Use a Linux-like shell, like "Git bash". Cygwin or Ubuntu shell should
also work. It is quite likely to NOT work with the shell included in
Atlassian SourceTree.

### Run locally

In the config directory, copy the sample config to your dev config file.
A gitignore setting prevents these from accidentally being checked in.

```
./serve.sh [config/another_config.yaml]
```

The app will be at http://localhost:8080.

Local admin console is available at http://localhost:8080. In order to
access rights in Storage, go to
http://localhost:8000/datastore?kind=PrincipalRights.

If you use the default dev_config (or don't provide the YAML file to
serve.sh), you will need to log in with @oasisdigital.com email. That's
the allowed reg exp in default YAML.

### Run on GAE

Each instance can have its own configuration, with different security
settings, feed data, and other settings.

To begin, create an application in AppEngine (using its web console),
giving it a unique ID.

Then create a configuration file for this application:

```
cp config/dev_config.yaml config/myconfig.yaml
```

The yaml file specifies application ID that should correspond to that
set in AppEngine. It also has configurable environment properties,
including:

* DATA_URL (query feed data URL)
* INSTANCE_LABEL (optional)

Create as many copies of it as you have applications.

Finally, to deploy code do AppEngine run:

```
./deploy.sh config/myconfig.yaml
```

### Production deployment

Kyle typically deploys each new instance to both the demo and production
applications:

```
./deploy.sh config/engagement-grid-demo.yaml
./deploy.sh config/engagement-grid-1265.yaml
```

These production config files contain keys to access our production
configuration data. It is both sensitive for our business purposes and
contains information for which we are under NDA, therefore the
production configuration is not kept in this project source repo.

### Initial configuration

Once the app is up and running, you can populate it with default assignee and
group configuration. To do so, log in as an admin user (in the sense of Google 
Cloud platform, not in-app right), and go to http://my-app/setup-defaults,
(e.g. http://localhost:8080/setup-defaults for local dev).

## Example

The OD production instance is deployed here:

https://training-grid-1265.appspot.com/#/

It allows access only to those configured for access.

