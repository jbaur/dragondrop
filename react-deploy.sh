#!/bin/bash

if [ "$1" == "" ]; then
  echo "Usage: ./deploy.sh config/training-grid-1265.yaml" >&2
  exit 1
fi

cat base_app.yaml $1 > app.yaml

GOAPP="goapp"

if [[ "$OSTYPE" == "msys" ]] || [[ "$OSTYPE" == "cygwin" ]] || [[ "$OSTYPE" == "win32" ]]; then
  echo "Looks like Windows, using goapp.bat for Go Appengine SDK"
  GOAPP="goapp.bat"
fi

cd server
echo "Gathering Go dependencies, ignore the warning about install location outside GOPATH"
echo
$GOAPP get
cd ..

set -e
cd react-app
yarn run build
cd ..

echo
echo "Deploying the app"
$GOAPP deploy
