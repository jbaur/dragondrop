#!/bin/bash

if [ "$1" == "" ]; then
  CONFIG=config/dev_config.yaml
else
  CONFIG=$1
fi

cat base_app.yaml $CONFIG > app.yaml

GOAPP="goapp"

if [[ "$OSTYPE" == "msys" ]] || [[ "$OSTYPE" == "cygwin" ]] || [[ "$OSTYPE" == "win32" ]]; then
  echo "Looks like Windows, using goapp.bat for Go Appengine SDK"
  GOAPP="goapp.bat"
fi

cd server
echo "Gathering Go dependencies, ignore the warning about install location outside GOPATH"
echo
$GOAPP get
cd ..

set -e
cd client
yarn run build
cd ..

echo
echo "Starting the app"
$GOAPP serve
